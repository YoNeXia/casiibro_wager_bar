# Casiibro Wager Bar

---
### Name
This project is named "CASIIBRO Wager Bar".

### Description
Wager Bar made for CASIIBRO streamers. Using basic Web languages (HTML, JS, CSS) to develop it.

### Version
This code is up-to-date with version "1.0.0".

### Code Quality
Using Gitlab CI/CD with SonarCloud to execute code quality tests.
Result is:

*In coming*

### Visuals
*In coming*

## Getting started

---
### Installation
The project is a simple web project with HTML, CSS and JS files. You just need to open the HTM file into your browser to see web page. Another solution is upload files to a local server, run it and use your localhost address.

But for streamers want use it into their overlay:

- Open **OBS**
- Create a **Browser**
- Add the path of HTML file into the parameter **URL**
- Click on **OK**

### Usage
The project can be used by Stake casino affiliates, getting wagers.

To use it on OBS, after followed instructions into **Installation**:

- Right-click on your element and click on **Interact**
- Change the currency with the interface in the popup

### Project status
Current project status is *INITIALIZATION*

### Support
For any report or bug detected, please use the [issues](https://gitlab.com/YoNeXia/casiibro_wager_bar/-/issues) section.

## Authors

---
### Owner
<div style="text-align: center;">
<img src="https://pbs.twimg.com/profile_images/1437406082926878722/dEo3BDBO_400x400.jpg" alt="logo" width="200"/>
</div>

Project owner is **YoNiTe** company. Find their projects here: [GitLab](https://gitlab.com/YoNeXia)

### Authors
- **YoNiTe** - Owner - [GitLab](https://gitlab.com/YoNeXia)
- **YoNeXia** - Developer - [GitLab](https://gitlab.com/YoNeXia)

*A project done for CASIIBRO streamers.*

### Contributors
- **CASIIBRO Team** - Artistic director - [Twitch](https://www.twitch.tv/casiibro)
